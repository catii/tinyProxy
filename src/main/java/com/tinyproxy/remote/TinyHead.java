package com.tinyproxy.remote;

import com.tinyproxy.common.Kits;

/**
 * Local tiny proxy transfer to remote tiny server data
 *
 * @author PanJun
 */
public class TinyHead {

    private String host;

    private int port;

    private byte[] sha;

    private long millis;

    private byte[] xtoken;

    private byte[] cont;

    private int partLen;

    public boolean handledHead() {
        return !Kits.isBlank(host) && port > 0;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public byte[] getSha() {
        return sha;
    }

    public void setSha(byte[] sha) {
        this.sha = sha;
    }

    public long getMillis() {
        return millis;
    }

    public void setMillis(long millis) {
        this.millis = millis;
    }

    public byte[] getXtoken() {
        return xtoken;
    }

    public void setXtoken(byte[] newToken) {
        this.xtoken = newToken;
    }

    public byte[] getCont() {
        return cont;
    }

    public void setCont(byte[] cont) {
        this.cont = cont;
    }

    public int getPartLen() {
        return partLen;
    }

    public void setPartLen(int partLen) {
        this.partLen = partLen;
    }

    @Override
    public String toString() {
        return "Head [host=" + host + ":" + port + ", xtoken-len=" + xtoken.length + "]";
    }

}

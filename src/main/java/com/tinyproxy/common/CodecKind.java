package com.tinyproxy.common;

public enum CodecKind {
    ENCRYPT, DECRYPT, NONE
}
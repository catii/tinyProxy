package com.tinyproxy.local.socks;

import io.netty.handler.codec.socksx.SocksMessage;
import io.netty.handler.codec.socksx.v4.DefaultSocks4CommandResponse;
import io.netty.handler.codec.socksx.v4.Socks4CommandRequest;
import io.netty.handler.codec.socksx.v4.Socks4CommandStatus;

class Sock4 extends TinySocks {

    private Socks4CommandRequest request;

    @Override
    public boolean invalid() {
        return false;
    }

    @Override
    public SocksMessage respSuccess() {
        return new DefaultSocks4CommandResponse(Socks4CommandStatus.SUCCESS);
    }

    @Override
    public SocksMessage respReject() {
        return new DefaultSocks4CommandResponse(Socks4CommandStatus.REJECTED_OR_FAILED);
    }

    @Override
    public String host() {
        return request.dstAddr();
    }

    @Override
    public int port() {
        return request.dstPort();
    }

    @Override
    protected TinySocks setMessage(SocksMessage msg) {
        request = (Socks4CommandRequest) msg;
        return this;
    }

}

package com.tinyproxy.local.socks;

import io.netty.handler.codec.socksx.SocksMessage;
import io.netty.handler.codec.socksx.v4.Socks4CommandRequest;
import io.netty.handler.codec.socksx.v5.Socks5CommandRequest;

public class TinySocks {

    public static TinySocks instance(SocksMessage msg) {
        if (msg instanceof Socks4CommandRequest) {
            return new Sock4().setMessage(msg);
        } else if (msg instanceof Socks5CommandRequest) {
            return new Sock5().setMessage(msg);
        } else {
            return new TinySocks();
        }
    }

    public boolean invalid() {
        return true;
    }

    public SocksMessage respSuccess() {
        return null;
    }

    public SocksMessage respReject() {
        return null;
    }

    protected TinySocks setMessage(SocksMessage msg) {
        return null;
    }

    public String host() {
        return null;
    }

    public int port() {
        return 0;
    }

}

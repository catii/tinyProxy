package com.tinyproxy.local.socks;

import io.netty.handler.codec.socksx.SocksMessage;
import io.netty.handler.codec.socksx.v5.DefaultSocks5CommandResponse;
import io.netty.handler.codec.socksx.v5.Socks5CommandRequest;
import io.netty.handler.codec.socksx.v5.Socks5CommandStatus;

class Sock5 extends TinySocks {

    private Socks5CommandRequest request;

    @Override
    public boolean invalid() {
        return false;
    }

    @Override
    public SocksMessage respSuccess() {
        return new DefaultSocks5CommandResponse(Socks5CommandStatus.SUCCESS, request.dstAddrType(), request.dstAddr(),
                request.dstPort());
    }

    @Override
    public SocksMessage respReject() {
        return new DefaultSocks5CommandResponse(Socks5CommandStatus.FAILURE, request.dstAddrType());
    }

    @Override
    public String host() {
        return request.dstAddr();
    }

    @Override
    public int port() {
        return request.dstPort();
    }

    @Override
    protected TinySocks setMessage(SocksMessage msg) {
        request = (Socks5CommandRequest) msg;
        return this;
    }

}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp {

    public partial class FormClient : Form {

        private TcpClient tcpClient;
        private NetworkStream stream;

        public FormClient() {
            InitializeComponent();
            tcpClient = new TcpClient();
            IPEndPoint ip = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 28848);
            tcpClient.Connect(ip);

            stream = tcpClient.GetStream();
        }

        private void FormClient_FormClosed(object sender, FormClosedEventArgs e) {
            this.Dispose();
            stream.Close();
            tcpClient.Close();
        }

        private void btnClose_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void btnSend_Click(object sender, EventArgs e) {
            byte[] bytes = Encoding.UTF8.GetBytes(textSend.Text);
            stream.Write(bytes);



            textSend.Text = "";
            byte[] bytes2 = new byte[800];
            int read = stream.Read(bytes2, 0, bytes2.Length);
            textReply.Text = Encoding.UTF8.GetString(bytes2);
        }
    }
}

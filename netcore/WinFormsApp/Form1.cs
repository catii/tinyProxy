using org.catii.socks5;
using System;
using System.Net;
using System.Net.Sockets;
using System.Numerics;
using System.Text;
using WinFormsApp.socks;

namespace WinFormsApp {

    public class ReadState {
        private NetworkStream _stream;
        private byte[] _buffer;
        public NetworkStream Stream { get => _stream; }

        public String clientInfo { get; set; }
        public byte[] Buffer { get { return _buffer; } }
        public ReadState(NetworkStream Stream, byte[] Buffer, string clientInfo) {
            this._stream = Stream;
            this._buffer = Buffer;
            this.clientInfo = clientInfo;
        }
    }

    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private Thread thread;
        private static readonly TcpListener Listener = new(IPAddress.Any, 28848);

        private void Form1_Load(object sender, EventArgs e) {
            Listener.Start();
            Listener.BeginAcceptTcpClient(acceptTcpClient, Listener);

            new Socks5Server(10080);

            byte[] x = new byte[254];
            for (int i = 0; i < x.Length; i++) {
                x[i] = (byte)((byte)i);
            }

            T.En(x);
            MessageBox.Show(String.Join(",", x));
            T.De(x);
            MessageBox.Show(String.Join(",", x));
        }

        private void acceptTcpClient(IAsyncResult o) {
            TcpClient client = Listener.EndAcceptTcpClient(o);
            string? clientInfo = client.Client?.RemoteEndPoint?.ToString();
            if (clientInfo == null) {
                return;
            }
            listBox1.Invoke(() => {
                listBox1.Items.Add(DateTime.Now + ":" + clientInfo + "上线啦2！！");
            });
            //syncSendReply(client, clientInfo);

            NetworkStream stream = client.GetStream();
            byte[] Buffer = new byte[1024];
            stream.BeginRead(Buffer, 0, Buffer.Length, ReadCallback, new ReadState(stream, Buffer, clientInfo));

            Listener.BeginAcceptTcpClient(acceptTcpClient, Listener);
        }


        private void ReadCallback(IAsyncResult o) {
            if (o.AsyncState is not ReadState state) {
                return;
            }
            int read = state.Stream.EndRead(o);
            if (read > 0) {
                String msg = Encoding.UTF8.GetString(state.Buffer, 0, read);

                String log = $"[{DateTime.Now}]：收到{state.clientInfo}|-> {msg}";

                char[] chars = msg.ToCharArray();
                Array.Reverse(chars);
                String reply = new string(chars);
                byte[] replyBytes = Encoding.UTF8.GetBytes(reply);
                //GOOD!!!! good !!
                //state.Stream.Write(replyBytes, 0, replyBytes.Length);
                //state.Stream.Flush();

                state.Stream.BeginWrite(replyBytes, 0, replyBytes.Length, WriteCallback, state);
                listBox1.Invoke(() => {
                    listBox1.Items.Add(log);
                });

                Array.Clear(state.Buffer, 0, state.Buffer.Length);
                state.Stream.BeginRead(state.Buffer, 0, 800, ReadCallback, state);
            } else {
                listBox1.Invoke(() => {
                    listBox1.Items.Add($"[{DateTime.Now}]：{state.clientInfo}【掉线啦】！");
                });
            }
        }

        private void WriteCallback(IAsyncResult o) {
            if (o.AsyncState is ReadState state) {
                listBox1.Invoke(() => {
                    listBox1.Items.Add($"[{DateTime.Now}]回复{state.clientInfo}消息完成！！");
                });
                state.Stream.EndWrite(o);
            }
        }


        long seed = 1;
        private void btnSend_Click(object sender, EventArgs e) {
            FormClient formClient = new FormClient();
            formClient.Text = "客户端：" + (seed++);
            formClient.Show();
        }
        private void syncSendReply(TcpClient client, string? clientInfo) {
            ThreadPool.QueueUserWorkItem(o => {
                NetworkStream Stream = client.GetStream();
                while (Stream != null) {
                    //异步读取数据
                    byte[] buffer = new byte[800];
                    int read = Stream.Read(buffer, 0, buffer.Length);
                    if (read > 0) {
                        string msg = Encoding.UTF8.GetString(buffer, 0, read);

                        char[] chars = msg.ToCharArray();
                        Array.Reverse(chars);
                        String reply = new string(chars);
                        byte[] replyBytes = Encoding.UTF8.GetBytes(reply);
                        Stream.Write(replyBytes, 0, replyBytes.Length);
                        String log = $"[{DateTime.Now}]收到{clientInfo}: {msg}";
                        listBox1.Invoke(() => {
                            listBox1.Items.Add(log);
                        });
                    } else {
                        String log = $"[{DateTime.Now}]{clientInfo}【掉线】";
                        listBox1.Invoke(() => {
                            listBox1.Items.Add(log);
                        });
                        break;
                    }

                }
            }, client);
        }
    }
}
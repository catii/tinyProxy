﻿namespace WinFormsApp {
    partial class FormClient {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            btnSend = new Button();
            textSend = new TextBox();
            label1 = new Label();
            btnClose = new Button();
            label2 = new Label();
            textReply = new TextBox();
            SuspendLayout();
            // 
            // btnSend
            // 
            btnSend.Location = new Point(95, 200);
            btnSend.Name = "btnSend";
            btnSend.Size = new Size(105, 29);
            btnSend.TabIndex = 0;
            btnSend.Text = "连接并发送";
            btnSend.UseVisualStyleBackColor = true;
            btnSend.Click += btnSend_Click;
            // 
            // textSend
            // 
            textSend.Location = new Point(34, 55);
            textSend.MaxLength = 128;
            textSend.Name = "textSend";
            textSend.Size = new Size(406, 27);
            textSend.TabIndex = 1;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(34, 20);
            label1.Name = "label1";
            label1.Size = new Size(84, 20);
            label1.TabIndex = 2;
            label1.Text = "发送的内容";
            // 
            // btnClose
            // 
            btnClose.Location = new Point(259, 200);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(94, 29);
            btnClose.TabIndex = 3;
            btnClose.Text = "关闭";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(34, 107);
            label2.Name = "label2";
            label2.Size = new Size(84, 20);
            label2.TabIndex = 4;
            label2.Text = "回复内容：";
            // 
            // textReply
            // 
            textReply.Location = new Point(34, 139);
            textReply.Name = "textReply";
            textReply.ReadOnly = true;
            textReply.Size = new Size(406, 27);
            textReply.TabIndex = 5;
            // 
            // FormClient
            // 
            AutoScaleDimensions = new SizeF(9F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(490, 266);
            Controls.Add(textReply);
            Controls.Add(label2);
            Controls.Add(btnClose);
            Controls.Add(label1);
            Controls.Add(textSend);
            Controls.Add(btnSend);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            Name = "FormClient";
            StartPosition = FormStartPosition.CenterParent;
            Text = "FormClient";
            FormClosed += FormClient_FormClosed;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnSend;
        private TextBox textSend;
        private Label label1;
        private Button btnClose;
        private Label label2;
        private TextBox textReply;
    }
}
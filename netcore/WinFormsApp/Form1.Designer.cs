﻿namespace WinFormsApp {
    partial class Form1 {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            btnFibEffect = new Button();
            listBox1 = new ListBox();
            btnOpenClient = new Button();
            SuspendLayout();
            // 
            // btnFibEffect
            // 
            btnFibEffect.Location = new Point(384, 409);
            btnFibEffect.Name = "btnFibEffect";
            btnFibEffect.Size = new Size(262, 29);
            btnFibEffect.TabIndex = 0;
            btnFibEffect.Text = "随便测试";
            btnFibEffect.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            listBox1.FormattingEnabled = true;
            listBox1.ItemHeight = 20;
            listBox1.Location = new Point(12, 12);
            listBox1.Name = "listBox1";
            listBox1.Size = new Size(776, 384);
            listBox1.TabIndex = 1;
            // 
            // btnOpenClient
            // 
            btnOpenClient.Location = new Point(148, 409);
            btnOpenClient.Name = "btnOpenClient";
            btnOpenClient.Size = new Size(94, 29);
            btnOpenClient.TabIndex = 2;
            btnOpenClient.Text = "打开客户端";
            btnOpenClient.UseVisualStyleBackColor = true;
            btnOpenClient.Click += btnSend_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(9F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(btnOpenClient);
            Controls.Add(listBox1);
            Controls.Add(btnFibEffect);
            Name = "Form1";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Form1";
            Load += Form1_Load;
            ResumeLayout(false);
        }

        #endregion

        private Button btnFibEffect;
        private ListBox listBox1;
        private Button btnSend;
        private Button btnOpenClient;
    }
}
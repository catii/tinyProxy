# tinyProxy

#### 介绍
本项目仅学习使用!!
#### 2023/10/28  catii
采用graalvm重构项目，生成目标平台可（目前仅linux和window64）执行程序，方便使用。同时降低对springboot依赖。
放弃对net core维护(源代码在netcore中)。

* 系统运行是会自动在可执行程序同目录下（不存在配置文件时），生成配置文件 **TinyProxy.cfg**
* TinyProxy.cfg介绍
~~~properties
## 远程服务器还是本地sock5服务器
remote=false
## 远程服务器地址
remoteHost=127.0.0.1
## 远程服务器端口
remotePort=34568
## 通讯令牌
token=dogee-htmoon

~~~

#### 2023/09/04  catii 
* 注：C#在实际使用中，交叉编译后的可执行程序运行效率太慢，在高延时场景无法使用，没有太多时间去诊断。
* 继续使用java技术栈进行优化
目前.net core可以支持跨平台，着手提供C#版本的类似实现，c#优势：
* 依托winform能力，可以在客户端提供图形化设置
* 编译成单体可执行发布件后，可以简化安装部署过程（主要是无jre诉求）
* 可能：C#对内存会更小，因为java需要springboot+netty；C#只需要language依赖就能完成

#### 原java技术栈介绍
本项目是Netty实战项目，你可以免费下载源码学习，用以熟悉netty基本api运用（包括：Future/ChannelHandler/Decoder/Listener等等）。

TinyProxy是sock5/sock4a上网代理项目， 分为本地客户端(local)和远程服务端(remote)，代码都是同一份。你可以通过命令行参数

本项目经典场景： 企业内部局域网计算机，通过一台可以上网服务器，完成局域网计算机访问互联网

**郑重声明：本项目是以技术学习、交流为主要目的，请自觉遵守互联网相关法律，请勿做非法用途！**

#### 软件架构

##### 总体工作流程

![image-20191118150559280](README.assets/image-20191118150559280.png)

**注:** 

* 本地服务器和远程需要使用相同的token（建议采用32字符的uuid），否则会连接失败
* Local和Remote采用二进制加密数据传输，保证了通讯安全

##### 项目使用的开源框架

* jar包管理：maven
* springboot-2.2.1：利用springboot最终生成胖jar，简化最终部署包的运行（java -jar xxx.jar）
* netty-all-4.1.43.Final.jar：利用netty的高性能，简单性；让项目能简单/可靠的使用java nio优势


#### 安装教程

不管是本地电脑，还是远程服务器，都需要安装jdk8以上的运行环境。你可以采用源代码编译，也可以直接使用Release/proxy-1.0.0.jar二进制包（推荐）。下面介绍二进制安装过程

1. 服务器安装

   * 把Release目录下的 proxy-1.0.0.jar和tinyRemote.bat下载并上传至服务器

   * 修改tinyRemote.bat启动参数

     javaw -jar proxy-1.0.0.jar --tiny.remote=true --tiny.remotePort=13080 --tiny.token=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

     **关键参数解释：**

      --tiny.remote=false，表示proxy-1.0.0.jar是以远程服务器的方式启动

     --tiny.token=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx，xxx部分指定你的服务端鉴权token

     --tiny.remotePort=13080 13080为服务端监听端口

2. 客户端

   * 把Release目录下的 proxy-1.0.0.jar和tinyLocal.bat下载本地

   * 修改tinyLocal.bat启动参数

     javaw -jar proxy-1.0.0.jar --tiny.remote=false --tiny.localPort=11080 --tiny.remotePort=13080 --tiny.remoteHost=192.168.168.168 --tiny.token=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

     **关键参数解释：**

      --tiny.localPort=11080，为本地sock5服务器监听端口，供浏览器代理服务器配置

     --tiny.remote=false，表示以本地代理服务器方式启动

     --tiny.remotePort=13080，表示远程服务器的监听端口，用来建立socket套接字

     --tiny.remoteHost=192.168.168.168，表示远程服务器IP或域名，根据自己实际情况修改

     --tiny.token=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx，指定服务端要求的鉴权token，用来与服务器端密文传输

     



#### 使用说明

1.  

#### 参与贡献

1.  


program proxy;

uses
  Forms,
  windows,
  uFrmMain in 'uFrmMain.pas' {MainForm};

{$R *.res}

var
  myMutex: HWND;
begin
  //CreateMutex建立互斥对象，并且给互斥对象起一个唯一的名字。
  myMutex := CreateMutex(nil, false, 'tiny-proxy-mutex');
  //程序没有被运行过
  if WaitForSingleObject(myMutex,0) <> WAIT_TIMEOUT then
  begin
    Application.Initialize;
    Application.MainFormOnTaskbar := False;
    Application.CreateForm(TMainForm, MainForm);
    Application.Run;
  end;
end.

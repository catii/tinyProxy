unit uFrmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, ShellApi, StdCtrls, TLHelp32;

type
  TMainForm = class(TForm)
    TrayIcon: TTrayIcon;
    trayMenu: TPopupMenu;
    miExit: TMenuItem;
    N2: TMenuItem;
    miShowHide: TMenuItem;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtHost: TEdit;
    edtPort: TEdit;
    edtToken: TEdit;
    btnEdit: TButton;
    btnCancel: TButton;
    Timer1: TTimer;
    procedure miExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure miShowHideClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnEditClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    procedure loadCfg;
    procedure toggleEditors(readOnly: Boolean);
    procedure startTinyProxy;
    procedure killTinyProxy;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;
  sei: TShellExecuteInfo;

implementation

{$R *.dfm}
procedure TMainForm.btnCancelClick(Sender: TObject);
begin
  loadCfg();
  toggleEditors(true);
end;

procedure TMainForm.btnEditClick(Sender: TObject);
var
  cfgTxt: TStringList;
begin
  if edtHost.ReadOnly then
  begin
    loadCfg();
    toggleEditors(not edtHost.ReadOnly);
  end
  else
  begin
    toggleEditors(not edtHost.ReadOnly);
    cfgTxt := TStringList.Create();
    try
      cfgTxt.Add('remote=false');
      cfgTxt.Add('remoteHost=' + Trim(edtHost.Text));
      cfgTxt.Add('remotePort=' + Trim(edtPort.Text));
      cfgTxt.Add('token=' + Trim(edtToken.Text));
      cfgTxt.SaveToFile(extractFilePath(Application.ExeName) + 'TinyProxy.cfg');
    finally
      cfgTxt.Free;
    end;

    startTinyProxy();
  end;
end;

procedure TMainForm.toggleEditors(readOnly: Boolean);
var
  color: TColor;
begin
  color := clWhite;
  if readOnly then
  begin
    color := TColor($DDDDDD);
    btnEdit.Caption := '�༭';
  end
  else
  begin
    btnEdit.Caption := '����';
  end;

  edtHost.ReadOnly := readOnly;
  edtPort.ReadOnly := readOnly;
  edtToken.ReadOnly := readOnly;
  edtHost.color := color;
  edtPort.color := color;
  edtToken.color := color;
end;

procedure TMainForm.startTinyProxy;
var
  ovExitCode: LongWord;
begin
  killTinyProxy();

  FillChar(sei, SizeOf(sei), 0);
  sei.cbSize := SizeOf(sei);
  sei.fMask := SEE_MASK_NOCLOSEPROCESS;
  sei.lpFile := 'tiny-proxy.exe';
  sei.lpParameters := '';
  sei.nShow := SW_HIDE;
  if not ShellExecuteEx(@sei) then
  begin
    ShowMessage('�޷���������');
  end;
end;

procedure TMainForm.killTinyProxy();
const
  PROCESS_TERMINATE=$0001;
var
  ContinueLoop: BOOL;
  FSnapshotHandle, hProcess: THandle;
  Entry: TProcessEntry32;
  ExeFileName: String;
begin
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  Entry.dwSize := Sizeof(Entry);
  ContinueLoop := Process32First(FSnapshotHandle,
                                 Entry);
  ExeFileName := UpperCase('tiny-proxy.exe');
  while integer(ContinueLoop) <> 0 do
  begin
    if ((UpperCase(ExtractFileName(Entry.szExeFile)) =  UpperCase(ExeFileName))
       or (UpperCase(Entry.szExeFile) =  UpperCase(ExeFileName))) then
    begin
      hProcess := OpenProcess(PROCESS_TERMINATE, BOOL(0), Entry.th32ProcessID);
      TerminateProcess(hProcess, 0);
    end;

    ContinueLoop := Process32Next(FSnapshotHandle, Entry);
  end;

  CloseHandle(FSnapshotHandle);
end;

procedure TMainForm.Timer1Timer(Sender: TObject);
begin
  Hide;
  Timer1.Enabled := False;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caNone;
  miShowHide.Click;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  toggleEditors(edtHost.ReadOnly);
  FillChar(sei, SizeOf(sei), 0);
  startTinyProxy;
  loadCfg();
end;

procedure TMainForm.loadCfg();
var
  cfgFile, S: String;
  k, v: String;
  cfgTxt: TStringList;
  I, p: Integer;
begin
  cfgFile := extractFilePath(Application.ExeName) + 'TinyProxy.cfg';
  if not FileExists(cfgFile) then
  begin
    Exit;
  end;

  cfgTxt := TStringList.Create;
  try
    cfgTxt.LoadFromFile(cfgFile);
    for I := 0 to cfgTxt.Count - 1 do
    begin
      S := Trim(cfgTxt[i]);
      p := Pos('=', S);
      if P < 1 then
        continue;

      k := Trim(Copy(S, 1, p - 1));
      v := Trim(Copy(S, p + 1, 99999));
      if k = 'remoteHost' then
         edtHost.Text := v
      else if k = 'remotePort' then
         edtPort.Text := v
      else if k = 'token' then
         edtToken.Text := v;
    end;
  finally
    cfgTxt.Free;
  end;
end;

procedure TMainForm.miExitClick(Sender: TObject);
begin
  killTinyProxy();

  Application.Terminate;
end;

procedure SwitchToThisWindow(hWnd:Thandle; fAltTab:boolean);stdcall;external 'User32.dll';

procedure TMainForm.miShowHideClick(Sender: TObject);
begin
  if Visible then
  begin
    Hide();
    miShowHide.Caption := '��ʾ(&D)';
  end
  else
  begin
    Show();
    BringToFront();
    SwitchToThisWindow(Handle, True);
    miShowHide.Caption := '����(&X)';
  end;
end;

end.
